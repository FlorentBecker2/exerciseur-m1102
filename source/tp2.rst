TP2 - Les conditionnelles
*************************

Jour-nuit
---------

Il fait jour de 7h à 19h, il fait nuit le reste du temps. C’est le
matin de 6h à 12h, l’après midi de 12h à 18h, la soirée de 18h à 21h,
la nuit le reste du temps. Écrire script Python qui permet, à partir
d'un entier compris entre 0 et 24, d'indiquer dans quelle partie de
la journée on est et s'il fait jour ou nuit. Pour éviter toute
ambiguïté, les intervalles de temps seront traités sous la forme:
heuredébut <= t < heurefin.
 
.. easypython:: /exercices/tp2/exojour-nuit.py
   :language: python
   :uuid: 123131

Sécurité routière
-----------------

D'après le site http://www.controleradar.org/contraventions.html voici la
réglementation en terme de contravention en cas d'excès de vitesse.

- Contravention pour dépassement de vitesse inférieur à 20 km/h

  - zones où la vitesse limitée est supérieure à 50 km/h
    
    Amende: 68 euros, Retrait 1 point, Suspension de permis : aucune

  - vitesse limitée inférieure à 50 km/h
    
    Amende : 135 euros, Retrait 1 point, Suspension de permis : aucune

- Contravention pour dépassement de vitesse entre 20 km/h et 30 km/h
  
  Amende: 135 euros, Retrait 2 points, Suspension de permis: aucune
  
- Contravention pour dépassement de vitesse entre 30 km/h et 40 km/h
  
  Amende  135 euros, Retrait 3 points, Suspension de permis: 3 ans
  
- Contravention pour dépassement de vitesse entre 40 km/h et 50 km/h
  
  Amende  135 euros, Retrait 4 points, Suspension de permis: 3 ans
  
- Contravention pour dépassement de vitesse de plus de 50 km/h
  
  Amende de 1500 euros, Retrait 6 points, Suspension de permis : 3 ans

On souhaiterait avoir une fonction qui nous donne les sanctions
encourues en fonction de l'excès de vitesse que l'on a commis.

.. easypython:: /exercices/tp2/exosecu-routiere.py
   :language: python
   :uuid: 12313

Qualification aux jeux olympiques
---------------------------------

La fédération d'athlétisme vient de publier les critères de qualification à l'épreuve du 100 m des jeux olympiques. Ces critères sont les suivants:
- Pour les hommes
  
  - soit il faut avoir un record personnel au 100m inférieur à 12 secondes et avoir gagné au moins 3 courses dans l'année
    
  - soit être champion du monde de la discipline
    
- Pour les femmes
  
  - soit il faut avoir un record personnel au 100m inférieur à 15 secondes et avoir gagné au moins 3 courses dans l'année
    
  - soit être championne du monde de la discipline

.. easypython:: /exercices/tp2/exoqualif-jo.py
   :language: python
   :uuid: 12313
