.. EasySphinx documentation master file, created by
   sphinx-quickstart on Thu Oct 20 12:10:31 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

M1102 Introduction à l'algorithmique et à la programmation
==========================================================

.. toctree::
   :maxdepth: 2
   :caption: M1102 Les sujets de TP
   
   tp2
   tp3
   tp4
   tp5



.. toctree::
   :maxdepth: 2
   :caption: M1102 Les sujets de TD
   
   td2
   td3
   td4
   td5



M1103 Structures de données
=============================

.. toctree::
   :maxdepth: 2
   :caption: M1103 - Exercices d'entrainement
   
   sdd
