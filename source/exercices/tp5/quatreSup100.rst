:orphan:

Exercice 5.1
------------

Écrire une fonction qui constitue la liste des quatre premiers éléments supérieurs (ou égaux) à 100 
dans une liste de nombres.

.. easypython:: /exercices/tp5/quatreSup100.py
   :language: python
   :uuid: 1231313
