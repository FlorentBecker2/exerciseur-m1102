:orphan:


Exercice 3
----------

Écrire une fonction qui détermine si un mot composé uniquement de lettres
minuscules non accentuées, contient plus de voyelles que de consonnes
ou vice versa. La fonction doit retourner -1 dans le premier cas, 1 dans le
second et 0 si il y a autant de voyelles que de consonnes dans le mot.
Par exemple, pour **demenagement** la fonction doit retourner **1**, pour
**bateau** elle doit retourner **-1**.

    
.. easypython:: /exercices/devoir1/exo3.py
   :language: python
   :uuid: 1231313
