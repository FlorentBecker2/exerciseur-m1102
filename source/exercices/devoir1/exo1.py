entrees_visibles = [(12,8,90,),
                    (15,10,45,),
                    (6,8,12,),                    
]
entrees_invisibles = [
        (10,10,90,),
        (12,12,43,),
        (10,12,10,),
        (10,25,18,),
]

@solution
def typeParallepipede (lon, larg, angle) :
    """
    Détermine si un parallépipède est un rectangle un carré un losange 
    ou quelconque 
    paramètres: lon (longueur) larg (largeur) angle (angle entre deux cotés)
    résultat: la chaine carré ou rectangle ou losange ou quelconque  
    """ 
    if angle == 90:
        if lon == larg :
            res = " carré "
        else :
            res = " rectangle "
    else :
        if lon == larg :
            res = " losange "
        else :
            res = " quelconque "
    return res

