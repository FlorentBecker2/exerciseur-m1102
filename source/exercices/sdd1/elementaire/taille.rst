Taille de la liste
-------------------------------

Écrire une fonction ``taille()`` qui prend en paramètre une liste et qui indique quelle est sa taille.


.. easypython:: taille.py
   :language: python
   :uuid: 1231313


