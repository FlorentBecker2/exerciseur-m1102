entrees_visibles=[
        ([1, 7, 42, -3, 6, 14]),
        ([8, 9, 5, 7, 14]),
        ([2, 4, -2, 9]),
]
entrees_invisibles=[
        ([-7, 8, 15, 61, -4, 7]),
        ([-7, -2, -3, -7]),
]


@solution
def moyenne(liste):
  return sum(liste)/len(liste)

