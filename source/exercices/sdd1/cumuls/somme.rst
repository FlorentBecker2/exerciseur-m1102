Somme
------------

Écrire une fonction ``somme`` qui prend en paramètre une liste de nombres et renvoie la somme de ces nombres.


.. easypython:: somme.py
   :language: python
   :uuid: 1231313

