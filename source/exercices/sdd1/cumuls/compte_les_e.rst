Combien de 'e' ?
---------------------------------

Écrire une fonction ``compte_les_e()`` qui prend en paramètre une chaîne de caractères et qui renvoie le nombre de ``e`` ou ``E`` que contient cette chaîne.


.. easypython:: compte_e.py
   :language: python
   :uuid: 1231313

