Moyenne
------------

Écrire une fonction ``moyenne`` qui prend en paramètre une liste de nombres et renvoie la moyenne de ces nombres.


.. easypython:: moyenne.py
   :language: python
   :uuid: 1231313

