La chaine contient-elle un 'e' ?
---------------------------------

Écrire une fonction ```contient_e()``` qui vérifie qu'une chaine de caractères contient la lettre 'e'.

.. easypython:: contient_e.py
   :language: python
   :uuid: 1231313


