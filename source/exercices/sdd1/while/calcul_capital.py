entrees_visibles=[
        (100,0),
        (1000,1),
        (1000,3),
]

entrees_invisibles=[ 
        (100,0),
]

@solution
def calcul_capital(capital_de_depart, nb_annee):
  return capital_de_depart*(1.05**nb_annee)
  
