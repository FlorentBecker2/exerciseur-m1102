Quand serais-je millionnaire ?
-----------------------------------

Hugo vient de gagner au loto et souhaite placer son argent sur un compte rémunéré. Le capital ainsi placé augmente chaque année de 5\% (c'est à dire qu'il est miultiplié par ``1.05``)


1. On veut écrire une fonction qui donne le capital disponible sur ce compte au bout de la `n` ième année.
   Ecrire une telle fonction fonction ``calcul_capital(capital_de_depart, nb_annee)`` 


.. easypython:: calcul_capital.py
   :language: python



2. Hugo se demande au bout de combien d'années il deviendra millionnaire, c'est à dire au bout de combien d'années le capital disponible aura dépassé 1 000 000 €.
   Ecrire une fonction qui permet de répondre à cette question.


.. easypython:: calcul_millionnaire.py
   :language: python

