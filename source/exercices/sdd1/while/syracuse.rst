La suite de Syracuse
---------------------

*Source : Wikipédia*

On appelle suite de Syracuse une suite d'entiers naturels définie de la manière suivante : on part d'un nombre entier plus grand que zéro ; s’il est pair, on le divise par 2 ; s’il est impair, on le multiplie par 3 et on ajoute 1. En répétant l’opération, on obtient une suite d'entiers positifs dont chacun ne dépend que de son prédécesseur.

La conjecture de Syracuse est l'hypothèse selone laquelle la suite de Syracuse de n'importe quel entier strictement positif atteint toujours la valeur 1.


Par exemple, à partir de 14, on construit la suite des nombres : ``[14, 7, 22, 11, 34, 17, 52, 26, 13, 40, 20, 10, 5, 16, 8, 4, 2, 1]``. C'est ce qu'on appelle la suite de Syracuse du nombre 14.

On s'arrête après que le nombre 1 ait été atteint.


1. Ecrire une fonction ``suivant`` qui prend un nombre entier plus grand que zéro en paramètre et qui renvoie le nombre suivant dans la suite de syracuse.


.. easypython:: syracuse_suivant.py
   :language: python




2. Quelle est la suite de Syracuse du nombre 3 ?


3. Ecrire une fonction ``suite_syracuse`` qui prend un nombre entier plus grand que zéro  en paramètre et qui renvoie sa suite de syracuse.


.. easypython:: syracuse_suite.py
   :language: python

