entrees_visibles = [(2001,),
                    (2012,),
                    (0,),
]
entrees_invisibles = [
        (1,),
        (12,),
        (11,),
        (112356,),
        (1001,),
        (12001,),
        (1001,),
        (1200,),
]

@solution
def deuxChiffresConsecutifsEgaux(nombre):
  """
  """
  liste=nombreToListe(nombre)
  for i in range(len(liste)-1):
    if liste[i]==liste[i+1]:
      return True
  return False


def nombreToListe(nombre):
  """
  """
  liste=[nombre%10]
  while nombre//10!=0:
    nombre=nombre//10
    liste.append(nombre%10)
  return liste
