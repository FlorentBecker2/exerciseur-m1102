adrian = { "nom": "Adrian", "courage": 9,"malice" : 10, "maison": "Serpentar"}
hermione = { "nom": "Hermione", "courage": 7, "malice" : 6, "maison":"Griffondor"}
luna = { "nom": "Luna", "courage": 2, "malice" : 2, "maison":"Serdaigle"}
marcus = { "nom": "Marcus", "courage": 6, "malice" : 10, "maison":"Serpentar"}
lavande = { "nom": "Lavande", "courage": 10, "malice" : 6, "maison":"Griffondor"}

a = { "nom": "a", "courage": 3, "malice" : 6, "maison":"Poufsouffle"}
b = { "nom": "b", "courage": 10, "malice" : 6, "maison":"Poufsouffle"}
c = { "nom": "c", "courage": 10, "malice" : 6, "maison":"Poufsouffle"}
d = { "nom": "d", "courage": 10, "malice" : 6, "maison":"Poufsouffle"}


promo1 = [adrian, hermione, luna, marcus, lavande]

promo2 = [adrian, hermione, luna, marcus, lavande, a, b, c, d]


entrees_visibles = [ promo1, ]
entrees_invisibles = [ promo2]

@solution
def tri_par_maison(promotion):
    res = {}
    for dico in promotion:
        if dico["maison"] in res :
            res[dico["maison"]].add(dico["nom"])
        else:
            res[dico["maison"]] ={dico["nom"]}
    return res

#  for e in entrees_visibles+entrees_invisibles:
    #  print(tri_par_maison(e))


