trois_amis = {
    'Camille' : {'Vélo', 'Kayak', 'Boxe'},
    'Dominique' : {'Vélo'},
    'Claude' : {'Lecture', 'Tricot', 'Boxe'}
    }

autre = {
    'a' : {'1', '2', '3'},
    'b' : {'4'},
    'a' : {'2', '3', '4'}
    }
    
    
entrees_visibles = [ trois_amis ]
entrees_invisibles = [ autre ]


@solution
def popularite_activite(amis):
    res = {}
    for amis , ensemble  in amis.items():
        for activite in ensemble:
            if activite in res:
                res[activite].add(amis)
            else:
                res[activite]= {amis}
    return res

#  for p in entrees_visibles+entrees_invisibles:
    #  print(popularite_activite(p))


