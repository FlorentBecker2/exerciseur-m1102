groupeA = {"Albert":17, "Bernard":14, "Chloé":17, "David":17, "Isidore":10}
groupeB = {"Farid":19, "Gérard":16, "Hernestine":18, "Justine":18}

a={'a': 1, 'b': 3, 'c': 5, 'd': 2, 'e': 12, 'f': 22}
r={'a':0, 'b':2, 'c':4, 'd':8, 'e':16, 'f':36}


entrees_visibles = [ groupeA, groupeB ]
entrees_invisibles = [a, r]


@solution
def moyenne_age(groupe) :
    return sum(groupe.values())/len(groupe)


#  for e in entrees_visibles+entrees_invisibles:
    #  print(moyenne_age(e))
