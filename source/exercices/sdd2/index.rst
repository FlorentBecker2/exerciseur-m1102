.. EasySphinx documentation master file, created by
   sphinx-quickstart on Thu Oct 20 12:10:31 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


Avec des dictionnaires
========================


.. toctree::
   :maxdepth: 1
   :glob:
   
   dico_simple/*


Structures de données imbriquées
====================================


.. toctree::
   :maxdepth: 1
   :glob:
   
   sdd_imbriquee/*

