entrees_visibles = [
            (1000,5,1100),
        (1000,3,1100),
        (0,5,11)
]
entrees_invisibles = [
        (1000,1500,5),
        (-10,150,5),
        (100,-100,5)
]

@solution
def investissement(somme,taux,objectif):
    """
    calcule le nombre d'année pour atteindre l'objectif si on place une somme
    à un certain taux
    paramètres: somme:    un nombre la somme initiale
                taux:     le taux d'intérêt annuel en % un nombre
                objectif: la somme objectif à atteindre un nombre
    résultat: le nombre d'années nécessaire pour atteindre l'objectif
    """
    # je choisis la boucle while car je ne sais pas combien de tour de boucle
    # j'aurai besoin
    if somme<=0 or taux<=0: # pour éviter les boucles infinies
        return None 
    capital=somme
    n=0
    while capital<=objectif:
    # invariant: capital est le capital acquis au bout de n années
        capital=capital*(1+taux/100)
        n=n+1
    return n
