entrees_visibles = [(["lundi","algorithme","bleu","oui","info"],["mardi","algorithme","","oui","info"]),
                    (["lundi","algorithme","bleu","oui","info"],["lundi","algorithme","bleu","oui","info"]),
                    (["lundi","algorithme"],["bleu","oui"])
]
entrees_invisibles = [
        ([],[]),
        (['a','b','c'],['a','b','d']),
        (['a','b','c'],['a','b','c'])
]

@solution
def correction(bonnesReponses, reponsesEtudiant):
    """
    Cette fonction calcule ne nombre de réponses correctes à un questionnaire
    en fonction d'une liste de bonnes réponses
    paramètres: bonnesReponses la liste des réponses attendues
                reponsesEtudiant la liste des réponses données par un étudiant
    résultat: le nombre de fois où la réponse de l'étudiant coincide avec la réponse attendue
    """
    #j'utilise un boucle for i in range car je dois parcourir deux listes en parallèle
    nbCorrectes=0
    for i in range(len(bonnesReponses)): 
    # nbCorrectes contient le nombre de fois où les deux listes coïncides sur les indices explorés
        if bonnesReponses[i]==reponsesEtudiant[i]:
            nbCorrectes+=1
    return nbCorrectes

