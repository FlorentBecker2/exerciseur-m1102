entrees_visibles = [([1,0,1,3,3,1,1], 3,), 
                    ([2,4,1,0,4,1,0,4], 4,), 
]

entrees_invisibles = [
                  ([2,2,1,0,2,1,0], 2,), 
                  ([2,4,1,0,2,4,0,3], 4,), 
                  ([2,4,1,3,4,1,2,5], 5,), 
                  ([4,1,0,4,1,0,4,3,2,2], 4,), 
]


@solution
def comptage(liste, N) : 
    """
    cette fonction permet de construire une liste de nombres entre 0 et N 
    avec le nombre de fois où l'élément apparait dans la liste 
    """  
    res = [] 
    for i in range(N + 1) : 
        res += [0]
    for j in liste : 
        res[j] +=  1
    return res
   
